package iface

import "context"

// Interface Definitions for A task, Station Task, Worker Task

type CmdMoveInterface interface {
	StartConsuming(ctx context.Context) error
	StopConsuming(ctx context.Context) error
	ReportToCtx(ctx context.Context) error
}
