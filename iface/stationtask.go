package iface

import "context"

type CmdStationInterface interface {
	RegisterToEngine(ctx context.Context) error
}
