package utils

import (
	"context"
	"fmt"

	"github.com/go-rod/rod/lib/launcher"
)

type RodProcess struct {
	Alive           bool
	ProcessLocation *launcher.Launcher
	ProcessURL      string
}

func NewRodProcess() *RodProcess {

	wg := RodProcess{Alive: false}

	return &wg
}

func (rp *RodProcess) TurnOn(ctx context.Context) error {

	al := rp.Checklive(ctx)
	if al {

		return nil

	}

	l := launcher.New()

	l = l.Headless(false)

	u, err := l.Launch()

	if err != nil {
		fmt.Println("error starting")

	}

	rp.ProcessURL = u

	rp.ProcessLocation = l

	rp.Alive = true

	return err

}

func (rp *RodProcess) Checklive(ctx context.Context) bool {
	return rp.Alive
}
