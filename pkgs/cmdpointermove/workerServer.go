package cmdpointermove

import (
	"context"
	"fmt"
	"local/ant1/pawnserver/common"
)

type PointerWorker struct {
	*common.WorkerServer

	ServerUrl   string
	Subject     string
	ConsumerTag string

	//funs

}

func NewPointerWorker(ctx context.Context, name string) (*PointerWorker, error) {

	queuename := name + common.PointerMove + "queue"
	ConsumerTag := name + common.PointerMove + "counsumer"

	workerserver, err := common.NewWorkServer(ctx, queuename, ConsumerTag)

	if err != nil {

		return nil, err
	}

	err = workerserver.Server.RegisterTasks(TasksMap)

	if err != nil {

		return nil, err
	}

	wg := PointerWorker{
		WorkerServer: workerserver,
	}

	return &wg, nil

}

func (pw *PointerWorker) StartConsumingServer(ctx context.Context) error {

	env := make(chan struct{})

	defer close(env)

	//engine := ctx.Value("engine")

	//value, ok := engine.(*common.Engine)

	// if !ok {

	// 	env <- struct{}{}

	// }

	// fmt.Sprint(value)
	// fmt.Sprint(ok)

	//errChan := make(chan error)

	err := pw.WorkerServer.Worker.Launch()

	go func() {

		for {

			select {
			case <-ctx.Done():
			//stop consuming here
			case <-env:
				fmt.Println("error Received")
				ctx.Done()
			}
		}

	}()

	return err
}
