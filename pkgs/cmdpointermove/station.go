package cmdpointermove

import (
	"context"
	"fmt"
	"local/ant1/pawnserver/utils"
	"reflect"
)

type PointerStation struct {
	//s	ProcessAlive bool
	localRod     *utils.RodProcess //Rod Process
	localBrowser *utils.Browser    //Browser
	CurrentTab   *utils.BrowserTab //BrowserTab
	AllBrowsers  utils.AllBrowsers
	AllTabs      utils.AllTabs
}

func NewPointerStation(ctx context.Context) (*PointerStation, error) {
	ps := PointerStation{}

	localrod := utils.NewRodProcess()

	// value := ctx.Value(common.EngineCtxString)

	// engine, ok := value.(*common.Engine)

	// if !ok {
	// 	// engine has disappeared from context

	// 	return nil, errors.New("engine has disappeared from ctxt")
	// }

	ps.localRod = localrod
	// ps.localBrowser = engine.CurrentBrowser
	// ps.CurrentTab = engine.CurrentTab
	// ps.AllTabs = engine.AllTabs
	// ps.AllBrowsers = engine.AllBrowser

	//ps.TurnOn(ctx)

	err := ps.InitAction(ctx)

	if err != nil {
		//
	}

	return &ps, nil
}

func (ps *PointerStation) TurnOn(ctx context.Context) error {

	fmt.Println("turn on is has been called")

	err := ps.localRod.TurnOn(ctx)

	// _, err := ps.localRod.ProcessLocation.Launch()

	// if err != nil {

	// 	//errors.Unwrap(err)

	// 	return errors.New("local rod process could not start")
	// }

	return err
}

func (ps *PointerStation) TurnOff(ctx context.Context) error {

	ps.localRod.ProcessLocation.Kill()

	//rod is off

	return nil
}

func (ps *PointerStation) RegisterToEngine(ctx context.Context) error {

	CurrentStationCtxString = reflect.TypeOf(ps).String()
	CurrentStationCtx = ctx
	//updating ps station to context

	// value := ctx.Value(common.IfaceEngineCtxString)

	// engine, ok := value.(*common.IfaceEngine)

	// if !ok {

	// 	return errors.New("IfaceEngine dissappeared from ctx")
	// }

	// err := engine.RegisterMe(ps)

	return nil

}

func (ps *PointerStation) InitAction(ctx context.Context) error {
	err := ps.RegisterToEngine(ctx)

	return err
}
