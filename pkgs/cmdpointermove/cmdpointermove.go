package cmdpointermove

import (
	"context"
	"errors"
	"local/ant1/pawnserver/common"
)

type CmdPointerMove struct {
	common.CmdMove

	Name string

	Station *PointerStation
	Worker  *PointerWorker

	Ctx context.Context

	LiveState bool

	//Retry bool
	//Coms string

	//WorkerFuncs map[type]type

}

// func (cm *CmdPointerMove) GetRetry(ctx context.Context) bool {

// 	return cm.Retry

// }

func NewCmdPointerMove(ctx context.Context, name string) (*CmdPointerMove, error) {

	outcmd := CmdPointerMove{}

	cmdglobal := common.NewCmdMove()

	station, err := NewPointerStation(ctx)

	if err != nil {
		//faied to create new pointerstation
		return nil, err
	}

	worker, err := NewPointerWorker(ctx, name)

	if err != nil {

		//failed to create worker

		ctx.Err()

		return nil, err
	}

	outcmd.Name = name
	outcmd.CmdMove = *cmdglobal
	outcmd.Ctx = ctx
	outcmd.Station = station
	outcmd.Worker = worker
	//outcmd.Retry = true

	err = outcmd.TurnOn(ctx)

	return &outcmd, err
}

func (cm *CmdPointerMove) ReportToCtx(ctx context.Context) error {

	value := ctx.Value(common.IfaceEngineCtxString)

	engine, ok := value.(*common.IfaceEngine)

	if !ok {
		//engine has disappeared from ctx

		return errors.New("engine has disappeared from ctx")
	}

	err := engine.RegisterMe(cm.Station)

	// engine.LocalRod = cm.Station.localRod
	// engine.CurrentBrowser = cm.Station.localBrowser
	// engine.CurrentTab = cm.Station.CurrentTab
	// engine.AllBrowser = cm.Station.AllBrowsers
	// engine.AllTabs = cm.Station.AllTabs

	return err

}

func (cm *CmdPointerMove) StartConsuming(ctx context.Context) error {

	//errchan := make(chan error)

	err := cm.Worker.StartConsumingServer(ctx)

	return err

	// if err != nil {

	// 	errchan <- err
	// }

	// go func() {

	// 	for {
	// 		select {

	// 		// case in := <-cm.CmdMove.GetStopChan():
	// 		// 	ctx.Done()

	// 		case <-errchan:
	// 			ctx.Done()

	// 		case <-ctx.Done():

	// 			//cm.Retry = false

	// 			break

	// 			//return false, <-errchan

	// 		}
	// 	}

	//}()

	//return cm.GetRetry(ctx), <-errchan

	//return true, <-errchan

	//return <-errchan
}

func (cm *CmdPointerMove) StopConsuming(ctx context.Context) error {

	err := cm.Worker.WorkerServer.StopWorking(ctx)

	return err
}

func (cm *CmdPointerMove) TurnOn(ctx context.Context) error {

	_ = cm.ReportToCtx(ctx)

	err := cm.Worker.WorkerServer.RegisterTasks(ctx, TasksMap)
	//collect funcs

	//add station to ctx
	return err
}
