package cmdpointermove

import (
	"context"
	"errors"
	"fmt"
	"local/ant1/pawnserver/common"
	"reflect"
)

// var tasksMap = map[string]interface{}{
// 	TurnOn: TurnOnFunc,
// }

var TasksMap = map[string]interface{}{

	TurnOn: TurnOnFunc,
	TurnOf: TurnOfFunc,
}

var CurrentStationCtx context.Context

var CurrentStationCtxString string

func TurnOnFunc() error {

	value := common.GlobalCtx.Value(common.IfaceEngineCtxString)

	engine, ok := value.(*common.IfaceEngine)

	if !ok {

		return errors.New("engine disappereaed from ctx")
	}

	mystation, err := engine.GetStation(CurrentStationCtxString)

	if err != nil {

		return errors.New("engine disappereaed from ctx")
	}

	val := reflect.ValueOf(mystation)

	station, ok := val.Interface().(*PointerStation)

	if !ok {

		return errors.New("engine disappereaed from ctx")
	}

	err = station.TurnOn(CurrentStationCtx)

	if err != nil {

		return errors.New("station in ctx mulfunctioned from ctx")
	}

	fmt.Println("Turn ON Has Happened")

	return err

}

func TurnOfFunc() error {

	fmt.Println("Turn OFF Has Happened")

	return nil
}
