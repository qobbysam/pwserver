package pkgs

import (
	"errors"
	"fmt"
	"reflect"

	"local/ant1/pawnserver/common"
	movecmd "local/ant1/pawnserver/iface"
	cmdmainmove "local/ant1/pawnserver/pkgs/cmdmainmove"
	cmdpointermove "local/ant1/pawnserver/pkgs/cmdpointermove"

	"github.com/lithammer/shortuuid"
	//	"github.com/RichardKnop/machinery/v1/tasks"
)

type PawnServer struct {
	//	mu   sync.Mutex
	Name string
	//SuperCtx    context.Context
	//RegistedCmd *sync.Map
	MainCmd  movecmd.CmdMoveInterface
	CmdMoves []movecmd.CmdMoveInterface

	Retry bool
}

func NewPawnServer() (*PawnServer, error) {

	wg := PawnServer{}
	wg.Name = shortuuid.New()

	//	global_ctx := context.Background()

	err := common.InitGlobalCtx()

	if err != nil {

		return nil, errors.New("failed to init global ctx")
	}
	maincmd, err := cmdmainmove.NewCmdMainMove(common.GlobalCtx, wg.Name)

	if err != nil {

		return nil, err
	}
	wg.CmdMoves = append(wg.CmdMoves, maincmd)

	pointercmdmove, err := cmdpointermove.NewCmdPointerMove(common.GlobalCtx.Context, "pointer")

	wg.CmdMoves = append(wg.CmdMoves, pointercmdmove)
	wg.MainCmd = maincmd

	// registercmdmap := map[string]interface{}{
	// 	"main":    maincmd,
	// 	"pointer": maincmd,
	// }

	// err = wg.RegisterTasks(registercmdmap)
	return &wg, err
}

func (pw *PawnServer) StartPawnServer() error {
	// collect all config files

	//create register cmds here

	//create Engine

	//start main cmd here

	//watch all cmds

	//common.InitGlobalCtx()

	ctx := common.GlobalCtx.Context

	err := pw.MainCmd.StartConsuming(ctx)

	// if !retry {

	// 	ctx.Done()

	// 	return err
	// }
	return err

}

// func (server *PawnServer) Get  y(ctx context.Context) bool {

// 	return server.GetRetry(ctx)

// }

func (server *PawnServer) RegisterTasks(namedCMDStructs map[string]interface{}) error {

	for _, cmd := range namedCMDStructs {
		if err := ValidateCMD(cmd); err != nil {
			return err
		}
	}

	// server.mu.Lock()
	// defer server.mu.Unlock()

	// for k, v := range namedCMDStructs {
	// 	server.RegistedCmd.Store(k, v)
	// }

	//server.broker.SetRegisteredTaskNames(server.GetRegisteredTaskNames())
	return nil
}

func ValidateCMD(cmd interface{}) error {
	v := reflect.ValueOf(cmd)
	t := v.Type()

	// Task must be a function
	fmt.Println(t.Kind())
	if t.Kind() != reflect.Ptr {
		return errors.New("cmds must be structs")
	}

	// Task must return at least a single value
	// if t.NumOut() < 1 {
	// 	return errors.New("cmds must return one value")
	// }

	// // Last return value must be error
	// lastReturnType := t.Out(t.NumOut() - 1)
	// errorInterface := reflect.TypeOf((*error)(nil)).Elem()
	// if !lastReturnType.Implements(errorInterface) {
	// 	return errors.New("last return value must be error")
	// }

	return nil
}
