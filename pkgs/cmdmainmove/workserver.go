package cmdmainmove

import (
	"context"
	"local/ant1/pawnserver/common"
)

type MainWorker struct {
	*common.WorkerServer
	ServerUrl   string
	Subject     string
	ConsumerTag string
}

func NewMainWorker(ctx context.Context, name string) (*MainWorker, error) {
	queuename := name + common.MainMove + "queue"
	consumertag := name + common.MainMove + "consumer"

	workserver, err := common.NewWorkServer(ctx, queuename, consumertag)

	if err != nil {
		return nil, err
	}

	err = workserver.Server.RegisterTasks(TasksMap)

	if err != nil {
		//some of the functions are brokern
		return nil, err
	}

	wg := MainWorker{
		WorkerServer: workserver,
	}

	return &wg, nil
}

func (mw *MainWorker) StartConsumingServer(ctx context.Context) error {
	env := make(chan struct{})

	defer close(env)

	err := mw.WorkerServer.Worker.Launch()

	//ctx error
	//ctx done

	return err
}
