package cmdmainmove

import (
	"context"
	"local/ant1/pawnserver/common"
)

type MainStation struct {
	FortAck       bool
	FortQueString string
	//UniqueName    string
	Engine *common.IfaceEngine
}

func NewMainStation(ctx context.Context, fortque string) (*MainStation, error) {

	ms := MainStation{}

	ms.FortAck = false
	ms.FortQueString = fortque
	//ms.UniqueName = shortuuid.New()

	return &ms, nil
}

func (ms *MainStation) RegisterFort(ctx context.Context) error {

	// get engine info strings (registered moves)

	return nil
}

func (ms *MainStation) RegisterToEngine(ctx context.Context) error {
	return nil
}
