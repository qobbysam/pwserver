package cmdmainmove

import (
	"errors"
	"local/ant1/pawnserver/common"
	"reflect"
)

var TasksMap = map[string]interface{}{
	RegisterFort: RegisterFortFunc,
}

var CurrentStationCtxString string

func RegisterFortFunc() error {

	return nil
}

func MSGforCmdFunc(msg []byte) error {

	value := common.GlobalCtx.Value(common.IfaceEngineCtxString)

	engine, ok := value.(*common.IfaceEngine)

	if !ok {

		return errors.New("engine disappereaed from ctx")
	}

	mystation, err := engine.GetStation(CurrentStationCtxString)

	if err != nil {
		return err
	}

	st := reflect.ValueOf(mystation)

	stt := st.Interface()

	myst, ok := stt.(*MainStation)

	if !ok {
		return errors.New("Station Has Dissappereaded")
	}

	err = myst.SendMSG(msg)

	if err != nil {

		return errors.New("MSG sending failed")
	}

	return err

}

func StartACmdFunc(movename string) error {

	return nil
}

func StopACmdFunc(movename string) error {
	return nil
}
