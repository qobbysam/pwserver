package cmdmainmove

import (
	"context"
	"errors"
	"local/ant1/pawnserver/common"
)

type CmdMainMove struct {
	*common.CmdMove
	Station   *MainStation
	Worker    *MainWorker
	LiveState bool
}

func NewCmdMainMove(ctx context.Context, servername string) (*CmdMainMove, error) {

	outcmd := CmdMainMove{}

	cmdbig := common.NewCmdMove()

	fortque := "somestring"

	station, err := NewMainStation(ctx, fortque)

	if err != nil {
		return nil, err
	}

	worker, err := NewMainWorker(ctx, servername)

	if err != nil {
		return nil, err
	}

	outcmd.CmdMove = cmdbig
	outcmd.Station = station
	outcmd.Worker = worker

	err = outcmd.TurnOn(ctx)
	return &outcmd, err
}

func (cm *CmdMainMove) ReportToCtx(ctx context.Context) error {

	value := ctx.Value(common.IfaceEngineCtxString)

	engine, ok := value.(*common.IfaceEngine)

	if !ok {
		//engine has disappeared from ctx

		return errors.New("engine has disappeared from ctx")
	}

	err := engine.RegisterMe(cm.Station)

	// engine.LocalRod = cm.Station.localRod
	// engine.CurrentBrowser = cm.Station.localBrowser
	// engine.CurrentTab = cm.Station.CurrentTab
	// engine.AllBrowser = cm.Station.AllBrowsers
	// engine.AllTabs = cm.Station.AllTabs

	return err

}

func (cm *CmdMainMove) TurnOn(ctx context.Context) error {

	_ = cm.ReportToCtx(ctx)

	err := cm.Worker.WorkerServer.RegisterTasks(ctx, TasksMap)
	//collect funcs

	//add station to ctx
	return err
}

func (cm *CmdMainMove) StartConsuming(ctx context.Context) error {
	err := cm.Worker.StartConsumingServer(ctx)

	return err
}

func (cm *CmdMainMove) StopConsuming(ctx context.Context) error {

	err := cm.Worker.WorkerServer.StartWorking(ctx)

	return err
}
