package main

import (
	"fmt"
	pkgs "local/ant1/pawnserver/pkgs"
)

// func main() {

// 	ctx := context.Background()

// 	pw := NewPawnServer(ctx)

// 	err := pw.StartPawnServer(ctx)

// 	if err != nil {

// 		fmt.Println(err)
// 	}
// }
func main() {

	//	ctx := context.Background()

	pw, err := pkgs.NewPawnServer()

	if err != nil {

		fmt.Println(err)
	}

	err = pw.StartPawnServer()

	if err != nil {

		fmt.Println(err)
	}
}
