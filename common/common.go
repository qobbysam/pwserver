package common

import (
	"context"
	"fmt"
)

const (
	EngineCtxString      = "engine"
	SuperConfigString    = "superconfig"
	IfaceEngineCtxString = "ifaceengine"
)

type SpecialCtx struct {
	context.Context
}

func (ctxs *SpecialCtx) Updateme(ctx context.Context, name, value interface{}) error {

	//newctx := context.Background()

	next_ctx := context.WithValue(ctx, name, value)

	ctxs.Context = next_ctx

	return nil

	//return nil

}

var GlobalCtx SpecialCtx

func InitGlobalCtx() error {

	superconfig := NewSuperConfig()

	_ = AddToGlobalCtx(GlobalCtx, SuperConfigString, superconfig)

	engine := NewEngine(GlobalCtx.Context)

	_ = AddToGlobalCtx(GlobalCtx.Context, EngineCtxString, engine)

	ifaceenengine := NewIfaceEngine()

	_ = AddToGlobalCtx(GlobalCtx.Context, IfaceEngineCtxString, ifaceenengine)

	return nil
}

func AddToGlobalCtx(ctx context.Context, name interface{}, value interface{}) error {

	err := GlobalCtx.Updateme(ctx, name, value)

	if err != nil {
		fmt.Println("gobalctx")
	}

	return err

}
