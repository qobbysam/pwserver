package common

import (
	"context"
	"errors"

	//"github.com/RichardKnop/machinery/v1"
	//"github.com/RichardKnop/machinery/v1"
	machinery "github.com/RichardKnop/machinery/v2"
	redisbackend "github.com/RichardKnop/machinery/v2/backends/redis"
	redisbroker "github.com/RichardKnop/machinery/v2/brokers/redis"
	machconfig "github.com/RichardKnop/machinery/v2/config"
	eagerlock "github.com/RichardKnop/machinery/v2/locks/eager"
)

type WorkerServer struct {
	Server machinery.Server
	Worker machinery.Worker
	//queue worker

	//all your task and funcs registered.
}

func NewWorkServer(ctx context.Context, queuename string, consumerTag string) (*WorkerServer, error) {

	value := ctx.Value(SuperConfigString)

	superconfig, ok := value.(*SuperConfig)

	if !ok {

		return nil, errors.New("superconfig dissappeared from ctx")
		// super config dissapperead from ctx
	}

	wg := WorkerServer{}

	new_server_config := &machconfig.Config{
		DefaultQueue:    queuename,
		ResultsExpireIn: 3600,
		Redis: &machconfig.RedisConfig{
			MaxIdle:                3,
			IdleTimeout:            240,
			ReadTimeout:            15,
			WriteTimeout:           15,
			ConnectTimeout:         15,
			NormalTasksPollPeriod:  1000,
			DelayedTasksPollPeriod: 500,
		},
	}
	//	:= new(machconfig.Config)

	broker := redisbroker.New(new_server_config, superconfig.RedisConfig.SocketPath, superconfig.RedisConfig.Host, superconfig.RedisConfig.Password, superconfig.RedisConfig.db)

	backend := redisbackend.New(new_server_config, superconfig.RedisConfig.SocketPath, superconfig.RedisConfig.Host, superconfig.RedisConfig.Password, superconfig.RedisConfig.db)
	lock := eagerlock.New()
	server := machinery.NewServer(new_server_config, broker, backend, lock)

	worker := server.NewWorker(consumerTag, 1)

	wg.Server = *server

	wg.Worker = *worker

	return &wg, nil
}

func (wk *WorkerServer) StartWorking(ctx context.Context) error {

	err := wk.Worker.Launch()

	return err
}

func (wk *WorkerServer) StopWorking(ctx context.Context) error {

	wk.Worker.Quit()

	return nil
}

func (wk *WorkerServer) RegisterTasks(ctx context.Context, taskfuncs map[string]interface{}) error {

	err := wk.Server.RegisterTasks(taskfuncs)

	return err
}
