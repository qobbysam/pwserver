package common

import (
	"context"
	"local/ant1/pawnserver/iface"
	utils "local/ant1/pawnserver/utils"
	"reflect"
	"sync"

	"github.com/lithammer/shortuuid"
)

//type AllBrowsers []*utils.Browser

//type AllTabs []*utils.BrowserTab

type RegisteredMoves []iface.CmdMoveInterface

type IfaceEngine struct {
	mu sync.Mutex

	UniqueName string

	Stations map[string]iface.CmdStationInterface
}

func (ife *IfaceEngine) RegisterMe(ifacestation iface.CmdStationInterface) error {

	ife.mu.Lock()

	defer ife.mu.Unlock()

	value := reflect.ValueOf(ifacestation)

	//err := ifacestation.RegisterToEngine(GlobalCtx)

	name := value.Type().String()

	ife.Stations[name] = ifacestation

	return nil

	//name.String()

}

func (ife *IfaceEngine) GetStation(name string) (iface.CmdStationInterface, error) {

	return ife.Stations[name], nil

}

func NewIfaceEngine() *IfaceEngine {

	return &IfaceEngine{Stations: make(map[string]iface.CmdStationInterface), UniqueName: shortuuid.New()}
}

type Engine struct {
	LocalRod        *utils.RodProcess
	CurrentBrowser  *utils.Browser
	CurrentTab      *utils.BrowserTab
	AllBrowser      utils.AllBrowsers
	AllTabs         utils.AllTabs
	RegisteredMoves RegisteredMoves
	CloseChan       chan struct{}
}

func NewEngine(ctx context.Context) *Engine {

	//get gaintconfig from ctx

	wg := Engine{}

	//localrod := utils.NewRodProcess()
	wg.CloseChan = make(chan struct{})

	//wg.LocalRod = localrod

	return &wg

}

func WatchEngine(engine *Engine) {

	for {
		_, ok := <-engine.CloseChan

		if !ok {

			break
		}

		//close all pawn processes
	}
}

// func AddEngineToCtx(en Engine, ctx context.Context) context.Context {

// 	//engine_str := "engine"

// 	new_ctx := context.WithValue(ctx, EngineCtxString, en)
// 	//ctx.Value(engine_str) = en

// 	return new_ctx

// }
