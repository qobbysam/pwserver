package common

type MoveToMoveMsg struct {
	FromMove    string
	ToMove      string
	Action      string
	ActionValue []string
}

type MainToChildrenMsg struct {
	To     string
	Action string
	Value  []string
}

type ChildtoChildMsg struct {
}

type PwToFortMsg struct{}

type FortToPwMsg struct{}
