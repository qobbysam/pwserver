package common

import (
	"encoding/json"

	"github.com/RichardKnop/machinery/v2/tasks"
)

type StationTask struct {

	// args needed for a station tasks

}

func CreateMSGforMoveTask(from, to, actionname string, actionvalue []string) (*tasks.Signature, error) {

	msg := MoveToMoveMsg{}

	msg.FromMove = from
	msg.ToMove = to
	msg.Action = actionname
	msg.ActionValue = actionvalue

	out, err := json.Marshal(msg)

	if err != nil {
		return nil, err
	}

	task_out := tasks.Signature{
		Name: "",

		Args: []tasks.Arg{
			// {	Name: "movename",
			// 	Type: "string",
			// 	Value: msg.ToMove ,
			// },

			{
				Name:  "msg",
				Type:  "byte",
				Value: out,
			},
		},
	}

	return &task_out, nil

}
