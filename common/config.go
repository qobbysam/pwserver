package common

type SuperConfig struct {
	RedisConfig *RedisConfig
}

type RedisConfig struct {
	//	host, password, socketPath string, db int

	Host       string
	Password   string
	SocketPath string
	db         int
}

func NewSuperConfig() *SuperConfig {

	return &SuperConfig{RedisConfig: &RedisConfig{
		SocketPath: "localhost:6379",
	}}
}
